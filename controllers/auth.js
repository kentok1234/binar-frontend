const axios = require('axios')
const jwt = require('jsonwebtoken')

async function authorize(req, res, next) {
    try {
        const token = req.cookies.access_token
        if(!token) {
            res.redirect('/login')
        }

        const payload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "secret")
        req.user = payload
        // req.user = await userService.get(payload.id.toString())

        next()
    }
    catch(err) {
        console.log(err)
        res.status(401).json({
            message: "Unauthorized",
        })
    }
}

module.exports = {
    authorize
}