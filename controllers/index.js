const express = require('express')
const main = require('./main')
const admin = require('./admin')
const auth = require('./auth')
const router = express.Router()

router.get('/', main.index)

router.get('/cars', main.cars)

router.get('/admin', auth.authorize, admin.index)

router.get('/admin/car',  auth.authorize, admin.addCar)

router.get('/admin/car/:id', auth.authorize, admin.getCar)

router.get('/login', (req, res) => {
    res.render('Login')
})

module.exports = router