
# Binar Front End

Repository yang hanya mengatur tampilan pada sisi front End


## Installation

Pertama install terlebih dahulu package yang dibutuhkan

```bash
  npm install
```

Project ini menggunakan nodemon untuk menjalankannya jadi harap install nodemon sebelum menjalankan aplikasi

Setelah itu ketikkan perintah berikut :
```bash
    npm start
```

## List Endpoint
- /
  Host utama
- /cars
  Menampilkan list mobil yang tersedia
- /admin
  Menampilkan dashboard admin
- /admin/car
  Menampilkan halaman untuk menambahkan data mobil
- /admin/car/:id
  Menampilkan profile dari mobil
- /login
  Menampilkan halaman login
