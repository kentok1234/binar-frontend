const carContainer = document.getElementById("car-container")
const headerEl = document.querySelector('header')
const navbarEl = document.getElementById('navbarToggle')
const buttonToggler = document.querySelector('.navbar-toggler')

const widthWindow = window.innerWidth
const parallaxCar = 0.2

window.addEventListener('scroll', (e) => {
    const widthWindow = window.innerWidth
    const scrollY = window.scrollY

    if (scrollY > 0) {
        headerEl.classList.add('scroll-header')
        carContainer.style.transform = 'translate(0%,' + scrollY * parallaxCar + 'px)'
        if (widthWindow > 992) {
            carContainer.style.transform = 'translateY(' + scrollY * parallaxCar + 'px)'
        }
    }
    else {
        if (!navbarEl.classList.contains('show')) {
            headerEl.classList.remove('scroll-header')
        }
    }
})

buttonToggler.addEventListener('click', () => {
    const scrollY = window.scrollY

    if (scrollY == 0) {
        headerEl.classList.toggle('scroll-header')
    }
})
