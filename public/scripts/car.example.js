class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="card p-4 shadow-sm rounded border-0 h-full justify-content-between">
        <div>
          <img src="${this.image}" alt="${this.manufacture}" class="car-image mb-3">
          <p>${this.manufacture}/${this.type}</p>
          <p class="fw-bold">Rp ${this.rentPerDay.toString().replace(/(.)(?=(\d{3})+$)/g,'$1.')} / Hari</p>
          <p>${this.description}</p>
          <p><i class="fa fa-users" aria-hidden="true"></i> ${this.capacity} orang</p>
          <p><i class="fa fa-cog" aria-hidden="true"></i> ${this.transmission}</p>
          <p><i class="fa fa-calendar-o" aria-hidden="true"></i> ${this.year}</p>
        </div>
        <button class="btn btn-success fw-bold">Pilih Mobil</button>
      </div>
    `;
  }
}
