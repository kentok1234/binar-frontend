const imageWomanFront = document.getElementById('girl-front')
const containerImageWoman = imageWomanFront.parentElement
const parentImageWoman = imageWomanFront.parentElement.parentElement
const widthMainWindow = window.innerWidth
let widthParentWoman = parentImageWoman.offsetWidth
let heightParentWoman = parentImageWoman.offsetHeight

// Position Image Girl
imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.5)) + 'px'
imageWomanFront.style.left = ((widthParentWoman - containerImageWoman.offsetWidth) + (containerImageWoman.offsetWidth / 2 * 0.74)) + 'px'

if ((widthMainWindow > 992) && (widthMainWindow < 1200)) {
    imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.45)) + 'px'
}

window.addEventListener('resize', () => {
    const scrollY = window.scrollY
    
    // when resize, change position image girl
    widthParentWoman = parentImageWoman.offsetWidth
    heightParentWoman = parentImageWoman.offsetHeight

    carContainer.style.transform = 'translate(0%,' + scrollY * parallaxCar + 'px)'
    if (widthMainWindow > 992) {
        carContainer.style.transform = 'translateY(' + scrollY * parallaxCar + 'px)'

        if (widthMainWindow < 1200) {
            imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.45)) + 'px'
            imageWomanFront.style.left = ((widthParentWoman - containerImageWoman.offsetWidth) + (containerImageWoman.offsetWidth / 2 * 0.74)) + 'px'
        } else {
            imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.5)) + 'px'
            imageWomanFront.style.left = ((widthParentWoman - containerImageWoman.offsetWidth) + (containerImageWoman.offsetWidth / 2 * 0.74)) + 'px'
        }
    } else {
        imageWomanFront.style.top = ((heightParentWoman - containerImageWoman.offsetHeight) + (containerImageWoman.offsetHeight / 2 * 0.5)) + 'px'
        imageWomanFront.style.left = ((widthParentWoman - containerImageWoman.offsetWidth) + (containerImageWoman.offsetWidth / 2 * 0.74)) + 'px'
    }

    
})

function toCars() {
    window.location.href = 'http://localhost:8080/cars'
}

