class App {
  constructor() {
    this.searchButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.dateRental = document.getElementById("date")
    this.timeRental = document.getElementById("time")
    this.capacityRental = document.getElementById("capacity")
  }

  async init() {
    await this.load();

    // Register click listener
    this.searchButton.onclick = this.run;
  }

  run = async () => {
    await this.load()
    this.clear()

    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.classList.add('col')
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });

  };

  async load() {
    const cars = await Binar.listCars((item) => {
      const dateRental = this.dateRental.value + ' ' + this.timeRental.value
      const capacity = this.capacityRental.value
      const date= new Date(dateRental)



      return item.available && item.availableAt.getTime() > date.getTime() && (capacity || 1) <= item.capacity
    });
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
